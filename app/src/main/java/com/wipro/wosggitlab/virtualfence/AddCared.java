package com.wipro.wosggitlab.virtualfence;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapFragment;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * The AddCared class is meant to handle the addition of Cared devices.
 * It controls layout which contains the entry fro cared devices ID and a portion of map to choose the location to make as geofence.
 *
 * @author sam
 * @version 1.0
 * @since 2015-07-25
 */

public class AddCared extends ActionBarActivity implements GoogleMap.OnInfoWindowClickListener {

    public static FragmentManager fragmentManager;
    private String TAG = "Sam";

    //Added to test map
    private LatLng defaultLatLng = new LatLng(39.233956, -77.484703);//Hard coding it, should change later
    private GoogleMap map;
    private int zoomLevel = 7;

    private Double currLatitude;
    private Double currLongitude;
    private LatLng latLng;
    private MarkerOptions markerOptions;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cared);
        /**
        Fragment fragment = new MapFragmentactivity();
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentactivity_map, fragment)
                    .commit();
        }
         **/

        //fragmentManager = getSupportFragmentManager();
        try {
            map = ((MapFragment) getFragmentManager().findFragmentById(R.id.location_map)).getMap();
            if (map!=null){
                map.getUiSettings().setCompassEnabled(true);
                map.setTrafficEnabled(true);
                map.setMyLocationEnabled(true);
                // Move the camera instantly to defaultLatLng.
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(defaultLatLng, zoomLevel));

                map.addMarker(new MarkerOptions().position(defaultLatLng)
                        .title("This is the title")
                        .snippet("This is the snippet within the InfoWindow"));
                        //.icon(BitmapDescriptorFactory
                        //        .fromResource(R.drawable.abc_ic_menu_cut_mtrl_alpha)));
                map.setOnInfoWindowClickListener(this);

                map.setOnMapClickListener(new OnMapClickListener(){

                    @Override
                    public void onMapClick(LatLng point){
                        latLng = point;
                        currLatitude = latLng.latitude;
                        currLongitude = latLng.longitude;

                        TextView textViewLatLon = (TextView)findViewById(R.id.text_lat_lon);
                        textViewLatLon.setText("lat:"+currLatitude+" , "+"lon:"+currLongitude);

                        Toast.makeText(getApplicationContext(),
                                currLatitude + ", " + currLongitude,
                                Toast.LENGTH_SHORT).show();

                        //Clears previously touched position
                        map.clear();

                        //Animating the touched position
                        map.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                        //Creating a marker and set position for marker
                        markerOptions = new MarkerOptions();
                        markerOptions.position(latLng);
                        map.addMarker(markerOptions);

                    }
                });
            }
        }catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_cared, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
    @Override
    public void onFragmentInteraction(Uri uri) {

    }
     **/
    @Override
    public void onPause() {
        if (map != null){
            map.setMyLocationEnabled(false);
            map.setTrafficEnabled(false);
        }
        super.onPause();
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

        Intent intent = new Intent(this, NewActivity.class);
        intent.putExtra("snippet", marker.getSnippet());
        intent.putExtra("title", marker.getTitle());
        intent.putExtra("position", marker.getPosition());
        startActivity(intent);
    }
}
