package com.wipro.wosggitlab.virtualfence.gcmServer.viewObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by sam on 16-08-2015.
 * Meant for POJO for creating JSON and perform complex operations
 * Needs Jackson API, which haven't implemented yet
 * So, just keeping it for later use
 */
public class Content implements Serializable {
    private List<String> registration_ids;
    private Map<String,String> data;

    public void addRegId(String regId){
        if(registration_ids == null)
            registration_ids = new LinkedList<>();
        registration_ids.add(regId);
    }

    public void createData(String title, String message) {
        if (data == null)
            data = new HashMap<>();

        data.put("title", title);
        data.put("message", message);
    }
}
