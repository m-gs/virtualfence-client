package com.wipro.wosggitlab.virtualfence;

import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.wipro.wosggitlab.virtualfence.gcmServer.PostToGcm;

import java.io.IOException;
import java.util.ArrayList;

public class CaredActivity extends ActionBarActivity implements OnCameraChangeListener {

    private GoogleMap map;
    ArrayList<Geofence> arraylist_geofences;
    ArrayList<LatLng> geoFenceCoordinates;
    ArrayList<Integer> geoRadius;

    private GeofenceStore geofenceStore;
    private final String TAG = "Sam";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG, "CaredAct - 1");
        setContentView(R.layout.activity_cared);
        Log.e(TAG, "CaredAct - 2");
        /**
         * Geofence
         */
        arraylist_geofences = new ArrayList<Geofence>();
        Log.e(TAG, "CaredAct - 3");
        geoFenceCoordinates = new ArrayList<LatLng>();
        geoRadius = new ArrayList<Integer>();
        Log.e(TAG, "CaredAct - 4");
        geoFenceCoordinates.add(new LatLng(22.586385, 88.429641));//22.584668, 88.429866));//22.592026, 88.429033));//
        Log.e(TAG, "CaredAct - 5");
        geoRadius.add(40);
        Log.e(TAG, "CaredAct - 6 =" + geoFenceCoordinates.get(0).latitude +" , "+ geoFenceCoordinates.get(0).longitude +" , " + geoRadius.get(0).intValue());
        arraylist_geofences.add(new Geofence.Builder().setRequestId("GeofenceID1")
                        .setCircularRegion(geoFenceCoordinates.get(0).latitude, geoFenceCoordinates.get(0).longitude, geoRadius.get(0).intValue())
                        .setExpirationDuration(Geofence.NEVER_EXPIRE)
                        .setLoiteringDelay(3000)
                        .setTransitionTypes(
                                Geofence.GEOFENCE_TRANSITION_ENTER
                                        | Geofence.GEOFENCE_TRANSITION_DWELL
                                        | Geofence.GEOFENCE_TRANSITION_EXIT).build()
        );

        Log.e(TAG, "In the onCreate - after making arrayist_geofences");

        /**
         * Add the geofence to GeoStore
         */
        geofenceStore = new GeofenceStore(this, arraylist_geofences);

        Log.e(TAG, "After calling geofenceStore");


        final Button buttonGetMonitoredSendGcmNotification = (Button) findViewById(R.id.button_get_monitored);
        buttonGetMonitoredSendGcmNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //postGcm();

            }
        });
    }

    /**
     * Used for Testing GCM notification from button click

    public void postGcm(){
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                Log.v(TAG, "Sending POST to GCM");
                //System.out.println( "Sending POST to GCM" );
                String apiKey = "AIzaSyCUu4OrYUrZCxzJBCXk4goDQPfBqBVt1zw";
                //Content content = createContent();
                PostToGcm.post(apiKey, "Testing Title");

                return null;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.v(TAG, "Done the post fron async");
            }
        }.execute(null, null, null);
    }
     **/

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        //geofenceStore.disconnect();
        super.onStop();
    }

    @Override
    protected void onResume(){
        Log.e(TAG, "CA - onResume - 1");
        super.onResume();

        Log.e(TAG, "CA - onResume - 2");
        if(GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {

            Log.e(TAG, "CA - onResume - 3");
            setupMapifNeeded();

            Log.e(TAG, "CA - onResume - 4");
        }
        else {

            Log.e(TAG, "CA - onResume - 5");
            GooglePlayServicesUtil.getErrorDialog(GooglePlayServicesUtil.isGooglePlayServicesAvailable(this), this, 0);

        }
    }

    private void setupMapifNeeded(){

        Log.e(TAG, "CA - seUpMapIfNeeded - 1");
        if (map == null) {
            Log.e(TAG, "CA - seUpMapIfNeeded - 2");
            map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.cared_map)).getMap();
            if(map != null) {
                Log.e(TAG, "CA - seUpMapIfNeeded - 3");
                setUpMap();
            }
        }
    }

    /**
     * Add markers or lines, add listeners or move the camera
     */

    private void setUpMap(){
        Log.e(TAG, "CA - seUpMap - 1");
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(22.584668, 88.429866), 14));

        Log.e(TAG, "CA - seUpMap - 2");
        map.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        Log.e(TAG, "CA - seUpMap - 3");
        map.setIndoorEnabled(false);

        Log.e(TAG, "CA - seUpMap - 4");
        map.setMyLocationEnabled(true);

        Log.e(TAG, "CA - seUpMap - 5");
        map.setOnCameraChangeListener(this);
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        Log.e(TAG, "CA - seUpMap - 6");
        for(int i=0; i < geoFenceCoordinates.size(); i++) {
            Log.e(TAG, "CA - seUpMap - 7");
            map.addCircle(new CircleOptions().center(geoFenceCoordinates.get(i))
                    .radius(geoRadius.get(i).intValue())
                    .fillColor(Color.GREEN)
                    .strokeColor(Color.TRANSPARENT).strokeWidth(2));
            Log.e(TAG, "CA - seUpMap - 8");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.e(TAG, "CA - seUpMap - 9");
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cared, menu);
        Log.e(TAG, "CA - seUpMap - 10");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Log.e(TAG, "CA - seUpMap - 11");
        //noinspection SimplifiableIfStatement
        Log.e(TAG, "CA - seUpMap - 12");
        if (id == R.id.action_settings) {
            return true;
        }
        Log.e(TAG, "CA - seUpMap - 13");
        return super.onOptionsItemSelected(item);
    }
}