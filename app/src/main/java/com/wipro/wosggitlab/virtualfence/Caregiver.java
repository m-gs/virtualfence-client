package com.wipro.wosggitlab.virtualfence;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;

/**
 * The Caregiver is the class to handle the layout of the Caregiver Home.
 * It also handles basic functionality to redirect to adding Cared devices.
 *
 * Performs registering to GCM server
 *
 * @author sam
 * @version 1.0
 * @since 2015-07-25
 */

public class Caregiver extends ActionBarActivity {

    private String TAG = "Sam";

    private GoogleCloudMessaging gcm;
    private String regId;
    private String PROJECT_NO = "648375628033";

    private Button button_addCaregiver;
    private  Button button_getGcmId;
    private  Button button_showGcmId;

    private TextView textGcmId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caregiver);

        button_addCaregiver = (Button) findViewById(R.id.button_add_cared);
        button_addCaregiver.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.e(TAG, "Logging Button");
                Intent caregiver_intent = new Intent(Caregiver.this, AddCared.class);
                startActivity(caregiver_intent);
            }
        });

        button_getGcmId = (Button) findViewById(R.id.button_register_gcm);
        button_getGcmId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "Registration Button click");
                getRegId();//To get registration iD
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_caregiver, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * getRegId method to request theGCM registration ID fro GCM server
     * Need to add functionality to store in database, or some static final string constant
     */
    public void getRegId(){
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                    }
                    regId = gcm.register(PROJECT_NO);
                    msg = "Device registered, registration ID=" + regId;
                    Log.v(TAG,  msg);

                } catch (IOException ex) {
                    msg = "Error = " + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                textGcmId = (TextView) findViewById(R.id.text_gcm_display);
                textGcmId.setText(msg);
            }
        }.execute(null, null, null);
    }
}
