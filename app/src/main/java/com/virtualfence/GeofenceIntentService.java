package com.virtualfence;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import com.virtualfence.Constants.ServerValues;
import com.virtualfence.serverCodes.ServerRequest;

import java.util.HashMap;

/**
 * Created by sam on 08-08-2015.
 *
 * Identifies Geofence changes and notifies itself
 * Also, sends GCM message using the PostToGcm class
 */
public class GeofenceIntentService extends IntentService{

    private String TAG = "Sam";
    private HashMap<String, String> hashedParams;


    private String gcmId="";// = "APA91bE-JALeni13h5vmoeIZMSDsMTU8rAWnL3zg8rTKWpSKgSmwFmfM7TUiZ2_rZKIadnXItLz6E2cTeQj4dYRVVoivVsi3MOCtUXXU62GKHCPJSbZKSmOzaU0CZx5qaBSVHREIcQNrGu_py1s9ucE3owgSSlm9Gg";
    private static final String USER_PREF = "UserDetails";
    private static final String DEFAULT = "N/A";

    public GeofenceIntentService() {
        super("GeofenceIntentService");
    }

    public void onCreate() {
        super.onCreate();

        //Get GCM id from SharedPreferences
        SharedPreferences sharedPreferences = getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);
        gcmId = sharedPreferences.getString("gcmId", DEFAULT);
    }
    public void onDestroy() {
        super.onCreate();
        Log.v(TAG, " GeofenceIntentS onDestroy");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.v(TAG, "GeofenceIntentS = onHandleIntent");
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);

        if( !geofencingEvent.hasError() ) {
            int transition = geofencingEvent.getGeofenceTransition();
            String notificationTitle = "VirtualFenceNotification";

            switch(transition) {
                case Geofence.GEOFENCE_TRANSITION_ENTER:
                    notificationTitle = "Geofence Entered";
                    Log.v(TAG, "Geofence Entered");
                    break;
                case Geofence.GEOFENCE_TRANSITION_EXIT:
                    notificationTitle = "Geofence Exited";
                    Log.v(TAG, "Geofence Entered");
                    break;
                case Geofence.GEOFENCE_TRANSITION_DWELL:
                    notificationTitle = "Geofence Dwell";
                    Log.v(TAG, "Geofence Entered");
                    break;
                default:
                    notificationTitle = "Geofence Unknown";
            }
            //Calls AsyncTask of sending GCM notification
            hashedParams = new HashMap<String, String>();
            hashedParams.put("gcmId", gcmId);
            hashedParams.put("title", notificationTitle);
            ServerRequest serverRequest = new ServerRequest();
            serverRequest.postNotification(ServerValues.SERVER_IP+"/notify", hashedParams);

            sendNotification(this, "Geofence Update...", notificationTitle);//Notifies itself
        }
        else {
            Log.e(TAG, "GIS - Error in onHandle");
        }
    }

    private void sendNotification( Context context, String notificationText, String notificationTitle ) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");

        wakeLock.acquire();
        Uri soundPath = Uri.parse("android.resource://" + getPackageName() + "/"+ R.raw.notification_2);
        Log.e(TAG, "Inside trying to send notification" + notificationText);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setContentTitle(notificationTitle)
                .setContentText(notificationText)
                .setSound(soundPath)
                .setSmallIcon(R.drawable.notif_icon)
                .setAutoCancel(false);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());

        wakeLock.release();

    }
}