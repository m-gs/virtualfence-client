package com.virtualfence;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

/**
 * Created by sam on 16-08-2015.
 * Intent Service to handle the message received during GCM notification, referred to by GcmBroadcastReceiver
 */
public class GcmMessageHandler extends IntentService{
    final String TAG = "Sam";
    private static final String USER_PREF = "UserDetails";
    private static final String DEFAULT = "N/A";

    String mes;
    private Handler handler;
    public GcmMessageHandler() {
        super("GcmMessageHandler");
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        handler = new Handler();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();

        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent received in BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        String isUpdate = extras.getString("is_update");

        Log.i(TAG, "Received : " + isUpdate);

        Log.i(TAG, "Received : " + extras.getString("title"));

        Log.i(TAG, "InBoolean "+Boolean.getBoolean(extras.getString("is_test")));


        if(isUpdate.equals("YES")){
            Log.i(TAG, "Received : (" + messageType + ")  " + extras.getString("title") +" isUpdate" +isUpdate);

            String fencename = extras.getString("fence_name");
            String latitude = extras.getString("latitude");
            String longitude = extras.getString("longitude");
            String radius = extras.getString("radius");
            String caregiverGcmid = extras.getString("caregiver_gcmid");


            Log.i(TAG, "Sent details are: "+fencename+" "+latitude+" "+longitude+" "+radius+" ");

            //Update SharedPreference for location information
            SharedPreferences sharedPreferences = getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("fencename", fencename);
            editor.putString("latitude", latitude);
            editor.putString("longitude", longitude);
            editor.putString("radius", radius);
            editor.putString("caregiver_gcmid", caregiverGcmid);
            editor.commit();
            Log.v(TAG, "Radius, Lat and lon save in sharedpref");

            showNotification(this, "Cordinates lat=" + latitude + " lon = " + longitude + " rad = " +radius, "Geofence Updated.");
        } else {
            mes = extras.getString("title");
            //showToast();
            //showDialog();

            showNotification(this, mes, "Geofence Alert");
            Log.i(TAG, "Received : (" + messageType + ")  " + extras.getString("title"));
        }
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    /**
    public void showToast(){
        handler.post(new Runnable() {
            public void run() {
                Toast.makeText(getApplicationContext(), mes, Toast.LENGTH_LONG).show();
            }
        });
    }
    **/

    private void showNotification( Context context, String notificationText, String notificationTitle ) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");

        wakeLock.acquire();

        Uri soundPath = Uri.parse("android.resource://" + getPackageName() + "/"+ R.raw.notification_2);
        Log.e(TAG, "Inside show notification" + notificationText);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setContentTitle(notificationTitle)
                .setContentText(notificationText)
                .setSound(soundPath)
                .setSmallIcon(R.drawable.notif_icon)
                //.setDefaults(Notification.DEFAULT_ALL)
                //.setDefaults(Notification.DEFAULT_SOUND|Notification.DEFAULT_LIGHTS|Notification.DEFAULT_VIBRATE)
                .setAutoCancel(false);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());

        wakeLock.release();
    }
}