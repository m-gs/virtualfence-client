package com.virtualfence;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.virtualfence.Constants.ServerValues;
import com.virtualfence.serverCodes.ServerRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Registration screen that offers registration via email/password.
 */
public class RegistrationActivity extends Activity implements LoaderCallbacks<Cursor> {

    /**
     * Keep track of the registration task to ensure we can cancel it if requested.
     */
    private UserRegistrationTask mAuthTask = null;

    private static final String PROJECT_NO = "648375628033";
    private static final String USER_PREF = "UserDetails";
    private static String DEFUALT = "N/A";
    private final String TAG = "Sam";
    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private EditText mConfirmPasswordView;
    private View mProgressView;
    private View mRegistrationFormView;

    private GoogleCloudMessaging gcm;
    private String gcmRegId;

    private HashMap<String, String> hashedParams;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        // Set up the registration form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();

        mPasswordView = (EditText) findViewById(R.id.password);
        mConfirmPasswordView = (EditText) findViewById(R.id.confirm_password);
        mConfirmPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.registration || id == EditorInfo.IME_NULL) {
                    attemptRegistration();
                    return true;
                }
                return false;
            }
        });

        Button registerButton = (Button) findViewById(R.id.email_register_button);
        registerButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v(TAG, "reg btn");
                attemptRegistration();
            }
        });

        mRegistrationFormView = findViewById(R.id.registration_form);
        mProgressView = findViewById(R.id.registration_progress);
    }

    private void populateAutoComplete() {
        getLoaderManager().initLoader(0, null, this);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptRegistration() {
        if (mAuthTask != null) {
            return;
        }

        try {
            // Reset errors.
            mEmailView.setError(null);
            mPasswordView.setError(null);

            // Store values at the time of the login attempt.
            String email = mEmailView.getText().toString();
            String password = mPasswordView.getText().toString();

            String confirmPassword = mConfirmPasswordView.getText().toString();

            boolean cancel = false;
            View focusView = null;

            // Check for a valid password, if the user entered one.
            if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
                mPasswordView.setError(getString(R.string.error_invalid_password));
                focusView = mPasswordView;
                cancel = true;
            } else if ((!TextUtils.isEmpty(confirmPassword) && !(confirmPassword.equals(password)))) {
                Log.v(TAG, "inside passwword compare" +confirmPassword +" "+password );
                mConfirmPasswordView.setError(getString(R.string.error_password_not_same));
                focusView = mConfirmPasswordView;
                cancel = true;
            }

            // Check for a valid email address.
            if (TextUtils.isEmpty(email)) {
                mEmailView.setError(getString(R.string.error_field_required));
                focusView = mEmailView;
                cancel = true;
            } else if (!isEmailValid(email)) {
                mEmailView.setError(getString(R.string.error_invalid_email));
                focusView = mEmailView;
                cancel = true;
            }

            if (cancel) {
                // There was an error; don't attempt registration and focus the first
                // form field with an error.
                focusView.requestFocus();
            } else {
                // Show a progress spinner, and kick off a background task to
                // perform the user login attempt.
                showProgress(true);
                Log.v(TAG, "Calling UserRegistration");
                mAuthTask = new UserRegistrationTask(email, password);
                mAuthTask.execute((Void) null);
            }
        } catch(Exception e){
            Log.v(TAG, "Exception in attemptRegistration ", e);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mRegistrationFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mRegistrationFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mRegistrationFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mRegistrationFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<String>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }


    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(RegistrationActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }

    /**
     * Represents an asynchronous registration task used to authenticate
     * the user.
     */
    public class UserRegistrationTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;
        private String jsonstr;
        UserRegistrationTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            Log.v(TAG, "Inside doInBackground");
            try {
                String email = mEmail;
                String password = mPassword;


                /*request theGCM registration ID from GCM server
                Need to add functionality to store in database, or some static final string constant
                */
                if (gcm == null) {
                    gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
                }
                gcmRegId = gcm.register(PROJECT_NO);
                Log.v(TAG, "Gcm resitartion: id= " + gcmRegId);

                if(TextUtils.isEmpty(gcmRegId)) {
                    return false;
                }


                hashedParams = new HashMap<String, String>();
                hashedParams.put("email", email);
                hashedParams.put("password", password);
                hashedParams.put("gcmId", gcmRegId);
                /*
                For sending registration request to server
                 */

                ServerRequest serverRequest = new ServerRequest();
                Log.v(TAG, "Inside doInBackground - calling serverRequest with"+ServerValues.SERVER_IP +"/register, " + hashedParams);
                JSONObject json = serverRequest.getJSONResponse(ServerValues.SERVER_IP+"/register", hashedParams);
                Log.v(TAG, "Callsed getJSONResponse, with JSON =" + json);
                if (json != null) {
                    Log.v(TAG, "Inside Loginactivity - json Not null");

                    jsonstr = json.getString("response");
                    Log.d(TAG, "Response text = " + jsonstr);

                    if (json.getBoolean("res")) {
                        //Toast.makeText(getApplicationContext(), "User registered successfully, please login to continue!", Toast.LENGTH_LONG).show();
                        Intent login_intent = new Intent(RegistrationActivity.this, LoginActivity.class);
                        startActivity(login_intent);
                    } ///else {
                       // Toast.makeText(getApplicationContext(), jsonstr, Toast.LENGTH_LONG).show();
                    //}
                } else {
                    Log.v(TAG, "Inside RegistrationActivity - json null");
                }

            }catch (JSONException e) {
                Log.e(TAG, "Exception", e);
                return false;
            } catch (IOException e) {
                Log.v(TAG, "GCM registration error occured. Try again later.", e);
                return false;
            } catch(Exception e ){
                Log.v(TAG, "GCM registration error, Try again later.", e);
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            Toast.makeText(getApplicationContext(), jsonstr, Toast.LENGTH_LONG).show();

            if (success) {
                //Save the GCM id, email and password in sharedPreferences
                SharedPreferences userDetails = getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = userDetails.edit();
                editor.putString("gcmId", gcmRegId);
                editor.putString("email", mEmail);
                editor.putString("password", mPassword);
                editor.commit();

                finish();

            } else {
                mPasswordView.setError(getString(R.string.retry));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

}

