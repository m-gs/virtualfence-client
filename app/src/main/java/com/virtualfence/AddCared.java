package com.virtualfence;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapFragment;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.virtualfence.Constants.ServerValues;
import com.virtualfence.serverCodes.ServerRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * The AddCared class is meant to handle the addition of Cared devices.
 * It controls layout which contains the entry fro cared devices ID and a portion of map to choose the location to make as geofence.
 *
 * @author sam
 * @version 1.0
 * @since 2015-07-25
 */

public class AddCared extends ActionBarActivity implements GoogleMap.OnInfoWindowClickListener {

    //Keep track of task
    private AddCaredTask addCaredTask = null;

    //UI references
    private NumberPicker numberPickerRadius;
    //private EditText editTextEmail;

    public static FragmentManager fragmentManager;
    private String TAG = "Sam";

    //Added to test map
    private LatLng defaultLatLng = new LatLng(39.233956, -77.484703);//Hard coding it, should change later
    private GoogleMap map;
    private int zoomLevel = 7;

    private Double currLatitude;
    private Double currLongitude;
    private int radius=40;//40 by default
    private LatLng latLng;
    private MarkerOptions markerOptions;

    private HashMap<String, String> hashedParams;
    private static final String USER_PREF = "UserDetails";
    private static final String DEFAULT = "N/A";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cared);
        //editTextEmail = (EditText)findViewById(R.id.text_caredUserEmail);

        numberPickerRadius = (NumberPicker)findViewById(R.id.numberPickerRadius);
        numberPickerRadius.setMinValue(10);
        numberPickerRadius.setMaxValue(500);
        /**
        Fragment fragment = new MapFragmentactivity();
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentactivity_map, fragment)
                    .commit();
        }
         **/

        //fragmentManager = getSupportFragmentManager();
        try {

            map = ((MapFragment) getFragmentManager().findFragmentById(R.id.location_map)).getMap();
            if (map!=null){
                map.getUiSettings().setCompassEnabled(true);
                map.setTrafficEnabled(true);
                map.setMyLocationEnabled(true);
                // Move the camera instantly to defaultLatLng.
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(defaultLatLng, zoomLevel));

                map.addMarker(new MarkerOptions().position(defaultLatLng)
                        .title("Geofence Location")
                        .snippet("Centre point of geofence."));
                        //.icon(BitmapDescriptorFactory
                        //        .fromResource(R.drawable.abc_ic_menu_cut_mtrl_alpha)));
                map.setOnInfoWindowClickListener(this);

                map.setOnMapClickListener(new OnMapClickListener(){

                    @Override
                    public void onMapClick(LatLng point){
                        latLng = point;
                        currLatitude = latLng.latitude;
                        currLongitude = latLng.longitude;

                        TextView textViewLat = (TextView)findViewById(R.id.text_lat);
                        textViewLat.setText("Latitude: " + currLatitude);

                        TextView textViewLon = (TextView)findViewById(R.id.text_lon);
                        textViewLon.setText("Latitude: " + currLongitude);

                        Toast.makeText(getApplicationContext(),
                                currLatitude + ", " + currLongitude,
                                Toast.LENGTH_SHORT).show();

                        //Clears previously touched position
                        map.clear();

                        //Animating the touched position
                        map.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                        //Creating a marker and set position for marker
                        markerOptions = new MarkerOptions();
                        markerOptions.position(latLng);
                        map.addMarker(markerOptions);

                    }
                });
            }

            Button buttonAddCared = (Button)findViewById(R.id.button_add_cared);
            buttonAddCared.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.v(TAG, "Clicked add button");
                    //Store values at time of attemp
                    String fence_name = "School";//Prelim check
                    //Get GCM id from SharedPreferences
                    SharedPreferences sharedPreferences = getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);
                    String gcmId = sharedPreferences.getString("gcmId", DEFAULT);

                    Bundle intent = getIntent().getExtras();
                    String email = intent.getString("caredEmail");
                    Log.v(TAG, "Email from intent = "+email);

                    radius = numberPickerRadius.getValue();//Get from NumberPicker
                    Log.v(TAG, "Calling AddCaredTask");
                    addCaredTask = new AddCaredTask(gcmId, email, fence_name, currLatitude, currLongitude, radius);
                    addCaredTask.execute();

                }
            });

        }catch (NullPointerException e) {
            Log.v(TAG, "NullPointerException", e);
        } catch(Exception e) {
            Log.v(TAG, "exception", e);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_cared, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
    @Override
    public void onFragmentInteraction(Uri uri) {

    }
     **/
    @Override
    public void onPause() {
        if (map != null){
            map.setMyLocationEnabled(false);
            map.setTrafficEnabled(false);
        }
        super.onPause();
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Intent intent = new Intent(this, NewActivity.class);
        intent.putExtra("snippet", marker.getSnippet());
        intent.putExtra("title", marker.getTitle());
        intent.putExtra("position", marker.getPosition());
        startActivity(intent);
    }


    public class AddCaredTask extends AsyncTask<Void, Void, Boolean> {

        private String mEmail="";
        private String mFenceName="";
        private double mLatitude=0;
        private double mLongitude=0;
        String gcmId="";
        private int mRadius=0;
        String jsonString;
        AddCaredTask(String gcm_id, String email, String fence_name, double latitude, double longitude, int radius ){
            try {
                gcmId = gcm_id;
                mEmail = email;
                mFenceName = fence_name;
                mLatitude = latitude;
                mLongitude = longitude;
                mRadius = radius;
            }catch(Exception e){
                Log.e(TAG, "Exception", e);
            }
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                String latitudeString = String.valueOf(mLatitude);
                String longitudeString = String.valueOf(mLongitude);
                String radiusString = String.valueOf(mRadius);


                hashedParams = new HashMap<String, String>();
                hashedParams.put("gcm_id", gcmId);
                hashedParams.put("email", mEmail);
                hashedParams.put("fence_name", mFenceName);
                hashedParams.put("latitude", latitudeString);
                hashedParams.put("longitude", longitudeString);
                hashedParams.put("radius", radiusString);

                ServerRequest serverRequest = new ServerRequest();
                JSONObject json = serverRequest.getJSONResponse(ServerValues.SERVER_IP+"/setgeofence", hashedParams);
                Log.v(TAG, "Calling request at"+ServerValues.SERVER_IP+"/setgeofence " +hashedParams );

                if (json != null){

                    jsonString = json.getString("response");
                    Log.v(TAG, "Json not null and returns "+jsonString);

                    if(json.getBoolean("res")) {
                        /*
                        //Update SharedPreference for location information
                        SharedPreferences sharedPreferences = getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("latitude", latitudeString);
                        editor.putString("longitude", longitudeString);
                        editor.putString("radius", radiusString);
                        editor.commit();
                        Log.v(TAG, "Radius, Lat and lon save in sharedpref");
                        */
                        finish();
                    }
                }
            } catch (JSONException e) {
                Log.v(TAG, "JSONException", e);
            } catch (Exception e) {
                Log.v(TAG, "Exception" , e);
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success){
            addCaredTask = null;
            Toast.makeText(getApplicationContext(), jsonString, Toast.LENGTH_LONG).show();
        }
    }

}
